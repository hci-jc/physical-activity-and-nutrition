# Physical-Activity-Regression (A Data-Centric Approach)

## Objective
- This project is an extension from a previous course project that uses adult nutrition data to examine the proportion of adults satisfying the CDC guidelines on muscle-strengthening activity.
- The current investigation uses a data-centric (instead of model-centric) approach that subjects the data to a thorough Exploratory Data Analysis (EDA) process before building predictive models.

## EDA & Data Cleaning
- Used column information to systematically exclude features with the following values:
	- Exceedingly large number of Nan's
	- All the values in a column are the same
	- Column values are composite of other column values
	- Duplicate columns either in values or in representation e.g., location coordinate & location name
- Individually ploted and examined the distribution of the response variable and the explanatory variables
- Used pair plots to visualize how the samples are spread with respect to feature pairs
	- e.g., feature1 and feature2 are spread on a linear pattern, indicating they are linearly related.
- Used pair plots to visulaize how each feature is related to the response variable
- Conducted correlation tests to illustrate the following:
	1) Numerical feature-to-feature relationships
	2) Correlation between numerical features and response variable

## Regression Models
- 2 input datasets were constructed.
	- full feature set: obtained via thorough EDA
	- subset: obtained based on the previous project's specification
- Ordinary Least Squares regression was used on the full feature set while additional algorithms were used on the subset (OLS, Lasso, Ridge, and ElasticNet)

## Results
- Results from the 10-fold cross-validation showed a drastic improvement using the full feature dataset compare to the subset. (see boxplots in .ipynb)
- The above comparison also translated to predictive power as OLS on a full feature set trounced all algorithms applied to the subset by 11%. (see error plots in .ipynb)
